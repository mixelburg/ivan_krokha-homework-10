#pragma once
#include "Item.h"
#include "Customer.h"
#include "MyUtil.h"

class Market {
public:
	Market();
	~Market();
	void addCustomer(std::string& name);
	void updateCustomer(std::string& name);
	void buyItems(std::string& name);
	void removeItems(std::string& name);
	void printItems();
	bool checkCustomer(std::string& name);
	void printUpdateMenu();
	void printBestCustomer();
private:
	std::map<std::string, Customer> customers;
	const int numItems = 10;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)
	};
};
