#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice) :
	_name(name), _serialNumber(serialNumber){
	setPrice(unitPrice);
	this->_count = 1;
}

Item::Item(const Item& other) :
	_name(other._name), _serialNumber(other._serialNumber),
	_unitPrice(other._unitPrice), _count(other._count)
{
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	return this->_serialNumber.compare(other._serialNumber) < 0;
}

bool Item::operator>(const Item& other) const
{
	return !(*this == other) && !(*this < other);
}

bool Item::operator==(const Item& other) const
{
	return this->_serialNumber.compare(other._serialNumber) == 0;
}

Item& Item::operator+=(const int& delta)
{
	this->_count += delta;
	if (this->_count < 1) throw BadCountException();
	return *this;
}

void Item::setPrice(const double& price)
{
	if (price <= 0) throw BadPriceException();
	this->_unitPrice = price;
}

std::ostream& operator<<(std::ostream& os, const Item& item)
{
	os << "{ " << "name=" << item._name << ", price=" << item._unitPrice << ", count=" << item._count << " }";
	return os;
}
