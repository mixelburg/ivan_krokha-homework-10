#pragma once
#include<iostream>
#include<string>
#include<algorithm>
#include<string>

#include "MyExceptions.h"

class Item
{
public:
	Item(std::string name, std::string serialNumber, double unitPrice);
	Item(const Item& other);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.
	Item& operator+=(const int& delta);
	friend std::ostream& operator<<(std::ostream& os, const Item& item);


	//get and set functions
	void setPrice(const double& price);

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!
};