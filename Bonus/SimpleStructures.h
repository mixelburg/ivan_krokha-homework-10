#pragma once

#include <list>
#include <unordered_set>
#include <set>
#include <vector>
#include <deque>
#include <string>

#include "Facade.h"

template <class T>
class SimpleLinkedList : public Facade<T> {
public:
	bool add(const T item) override {
		try
		{
			this->data.push_back(item);
			return true;
		}
		catch (const std::exception&)
		{
			return false;
		}
	}

	bool contains(const T item) const override {
		return std::find(this->data.begin(), this->data.end(), item) != this->data.end();
	}

	SimpleLinkedList() : Facade<T>(){
		this->name = "SimpleLinkedList";
	}
private:
	std::list<T> data;
};

template <class T>
class SimpleHashMap : public Facade<T> {
public:
	bool add(const T item) override {
		try
		{
			this->data.insert(item);
			return true;
		}
		catch (const std::exception&)
		{
			return false;
		}
	}

	bool contains(const T item) const override {
		return this->data.find(item) != this->data.end();
	}

	SimpleHashMap() : Facade<T>() {
		this->name = "SimpleHashMap";
	}
private:
	std::unordered_set<T> data;
};

template <class T>
class SimpleTreeSet : public Facade<T> {
public:
	bool add(const T item) override {
		try
		{
			this->data.insert(item);
			return true;
		}
		catch (const std::exception&)
		{
			return false;
		}
	}

	bool contains(const T item) const override {
		return this->data.find(item) != this->data.end();
	}

	SimpleTreeSet() : Facade<T>() {
		this->name = "SimpleTreeSet";
	}
private:
	std::set<T> data;
};

template <class T>
class SimpleVector : public Facade<T> {
public:
	bool add(const T item) override {
		try
		{
			this->data.push_back(item);
			return true;
		}
		catch (const std::exception&)
		{
			return false;
		}
	}

	bool contains(const T item) const override {
		return std::find(this->data.begin(), this->data.end(), item) != this->data.end();
	}

	SimpleVector() : Facade<T>() {
		this->name = "SimpleVector";
	}
private:
	std::vector<T> data;
};

template <class T>
class SimpleDeque : public Facade<T> {
public:
	bool add(const T item) override {
		try
		{
			this->data.push_back(item);
			return true;
		}
		catch (const std::exception&)
		{
			return false;
		}
	}

	bool contains(const T item) const override {
		return std::find(this->data.begin(), this->data.end(), item) != this->data.end();
	}

	SimpleDeque() : Facade<T>() {
		this->name = "SimpleDeque";
	}
private:
	std::deque<T> data;
};


