#pragma once

#include <string>

template<class T>
class Facade {
public:
	virtual bool add(const T item) = 0;
	virtual bool contains(const T item) const = 0;
	std::string name;
};
