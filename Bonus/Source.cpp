#include <fstream>
#include <iostream>
#include <windows.h>
#include <string>
#include <chrono>

#include "SimpleStructures.h"


int getNumLines(std::string fName) {
	std::ifstream myfile(fName);

	// new lines will be skipped unless we stop it from happening:    
	myfile.unsetf(std::ios_base::skipws);

	// count the newlines with an algorithm specialized for counting:
	int line_count = std::count(
		std::istream_iterator<char>(myfile),
		std::istream_iterator<char>(),
		'\n');

	std::cout << "Lines: " << line_count << "\n";
	return line_count;
}

void writeData(Facade<int>* dataContainer, std::string& dataFilePath, bool verbose, std::ofstream& LogFile, int data=0) {
	// get num lines in file
	int numLines = getNumLines(dataFilePath);
	int count = 1;

	int val;
	std::ifstream infile(dataFilePath);
	while (infile >> val)
	{
		if (verbose) std::cout << "[+] Adding value " << count << " / " << numLines << " : " << val << std::endl;
		count++;
		dataContainer->add(val);
	}
}

void searchData(Facade<int>* dataContainer, std::string& dataFilePath, bool verbos, std::ofstream& LogFile, int data) {
	bool found = dataContainer->contains(data);
	if (found) LogFile << "Found - " << data << std::endl;
	else LogFile << "Not found - " << data << std::endl;
}

void timeIt(void(*f)(Facade<int>* dataContainer, std::string& dataFilePath, bool verbose, std::ofstream& LogFile, int data),
	Facade<int>* dataContainer, std::string& dataFilePath, bool verbose,
	std::ofstream& LogFile, std::string what, int data=0) {
	LogFile << what << std::endl;

	// get start time
	auto start = std::chrono::steady_clock::now();

	(*f)(dataContainer, dataFilePath, verbose, LogFile, data);

	// get end time
	auto end = std::chrono::steady_clock::now();
	// display time delta
	std::cout << "[+] Elapsed time in microseconds : "
		<< std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
		<< " ms" << std::endl << std::endl;

	LogFile << "Elapsed time in microseconds : "
		<< std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
		<< " ms" << std::endl;
}

int main(int argc, char* argv[]) {
	std::string dataFileNames[] = {
	"data1.txt",
	"data2.txt",
	};

	std::string programPath = argv[0];
	programPath = programPath.substr(0, programPath.find_last_of("\\/"));
	bool displayAddInfo = false;
	const int numContainers = 5;

	for (std::string dataFileName : dataFileNames) {
		std::string dataFilePath = programPath + "/" + dataFileName;
		std::string resultsFileName = programPath + "/results-" + dataFileName;

		Facade<int>* dataContainers[numContainers];
		dataContainers[0] = new SimpleLinkedList<int>();
		dataContainers[1] = new SimpleHashMap<int>();
		dataContainers[2] = new SimpleTreeSet<int>();
		dataContainers[3] = new SimpleVector<int>();
		dataContainers[4] = new SimpleDeque<int>();

		std::cout << "[+] Opening file " << resultsFileName << " for logging" << std::endl << std::endl;
		std::ofstream LogFile(resultsFileName);

		for (Facade<int>* container : dataContainers) {
			std::cout << "[+] Running container : " << container->name << std::endl;
			LogFile << "Running container : " << container->name << std::endl;

			timeIt(&writeData, container, dataFilePath, displayAddInfo, LogFile, "Write data: ");
			timeIt(&searchData, container, dataFilePath, displayAddInfo, LogFile, "Search data: ", 5);
			timeIt(&searchData, container, dataFilePath, displayAddInfo, LogFile, "Search data: ", INT_MAX);
			LogFile << std::endl;
		}

		std::cout << std::endl << "[+] Closing results file..." << std::endl;
		LogFile.close();
		std::cout << "[+] Results file closed" << std::endl << std::endl;
	}


	std::cout << std::endl << "[+] Exiting..." << std::endl;
	return 0;
}